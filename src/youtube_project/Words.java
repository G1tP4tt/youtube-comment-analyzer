package youtube_project;

public class Words {
	private String theWord;
	private int wordNo;
	private double valenceMean;
	private double valenceSD;
	private double arousalMean;
	private double arousalSD;
	private double dominanceMean;
	private double dominanceSD;
	private String wordFrequency;

	// public String getTheWords() {
	// return this.theWords;
	// }
	public Words(String theWord, int wordNo, double valenceMean, double valenceSD, double arousalMean, double arousalSD,
			double dominanceMean, double dominanceSD, String wordFrequency) {
		this.theWord = theWord;
		this.wordNo = wordNo;
		this.valenceMean = valenceMean;
		this.valenceSD = valenceSD;
		this.arousalMean = arousalMean;
		this.arousalSD = arousalSD;
		this.dominanceMean = dominanceMean;
		this.dominanceSD = dominanceSD;
		this.wordFrequency = wordFrequency;
	}

	// create an empty constructor to later fill it up
	public Words() {
		// TODO Auto-generated constructor stub
	}

	public String getTheWord() {
		return this.theWord;
	}

	public int getWordNo() {
		return this.wordNo;
	}

	public double getValenceMean() {
		return this.valenceMean;
	}

	public double getValenceSD() {
		return this.valenceSD;
	}

	public double getArousalMean() {
		return this.arousalMean;
	}

	public double getArousalSD() {
		return this.arousalSD;
	}

	public double getDominanceMean() {
		return this.dominanceMean;
	}

	public double getDominanceSD() {
		return this.dominanceSD;
	}

	public String getWordFrequency() {
		return this.wordFrequency;
	}

	/*
	 * public static void FileReader(String inputFile) { String csvFile =
	 * "inputFile"; String line = ""; String cvsSplitBy = ",";
	 * 
	 * try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
	 * 
	 * while ((line = br.readLine()) != null) {
	 * 
	 * // use comma as separator String[] word = line.split(cvsSplitBy);
	 * ArrayList<Words> theObject = new ArrayList<Words>(); /* Words theObject =
	 * new Words(); theObject.theWords = word[0]; theObject.wordNo = word[1];
	 * theObject.valenceMean = word[2]; theObject.valenceSD = word[3];
	 * theObject.arousalMean = word[4]; theObject.arousalSD = word[5];
	 * theObject.dominanceMean = word[6]; theObject.dominanceSD = word[7];
	 * theObject.wordFreduency = word[8];
	 */
	// i need an arraylist of words and fill them up with the get method at i
	// turn them all to string but dont forget to turn the values back to ints
	// when calculating with the formulas
	/*
	 * for(int i = 1; i < word.length; i++) { theObject.set }
	 * System.out.println("[ " + word[0] + ", " + word[1] + ", " + word[2] +
	 * ", " + word[3] + ", " + word[4] + ", " + word[5] + ", " + word[6] + ", "
	 * + word[7] + ", "+ word[8] + " ]");
	 * 
	 * }
	 * 
	 * } catch (IOException e) { e.printStackTrace(); }
	 */

	public String toString() {

		return theWord + " | " + wordNo + " | " + valenceMean + " | " + valenceSD + " | " + arousalMean + " | "
				+ arousalSD + " | " + dominanceMean + " | " + dominanceSD + " | " + wordFrequency + "\n";
	}
}
