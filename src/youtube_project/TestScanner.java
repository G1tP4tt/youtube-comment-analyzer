package youtube_project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import java.net.URL;

public class TestScanner {
	/*
	 * public static void main(String[] args) throws FileNotFoundException {
	 * Scanner scanner = new Scanner(new
	 * File("/Users/dilshodbobokalonov/downloads/all.csv"));
	 * scanner.useDelimiter(","); while(scanner.hasNext()){
	 * System.out.print(scanner.next()+"|"); } scanner.close(); }
	 */
	
	public static void main(String[] args) {
		
		//No more fucking around with absoulte paths as long as the csv is in the same package
		URL url = WordFile.class.getResource("all.csv");
		File file = new File(url.getPath());
		
		WordFile tester = new WordFile();
		tester.addAll(file.getPath());

		Map<String, Words> wordMap = new HashMap<>();

		
			for (Words w : tester.getFile()) {

				// Fill Hash Map here
				// The HashMap is better than binary search. and very easy to handle...
				// We dont need the myFile.txt anymore because all words + values are saved in the HashMap
				// You save time cause we dont have to sort.
				// Plus we save time on every search attempt compared to all other possible data structures.
				wordMap.put(w.getTheWord(), w);
			}
		
		
		
		tester.doPatriceStuff(wordMap);
		

	}



}

