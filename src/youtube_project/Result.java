package youtube_project;

/**
 * The Result class, saves the total number of the values like  < -- ValenceMean -- > etc..
 * 
 * @author Patrice
 *
 */
public class Result {
	

	// Values to sum up
	private double totalValenceMean = 0;
	private double totalValenceSD = 0;
	private double totalArousalMean = 0;
	private double totalArousalSD = 0;
	private double totalDominanceMean = 0;
	private double totalDominanceSD = 0;
	// Absolute Number of counted
	private int wordCount;
	

	/**
	 * This method adds the values of a specific word to the total word-values found in the file.
	 * @param wordToAdd --> a word which got found in theWord HashMap
	 */
	public void addUp(Words wordToAdd) {
		// TODO Auto-generated method stub
		this.totalValenceMean += wordToAdd.getValenceMean();
		this.totalValenceSD += wordToAdd.getValenceSD();
		this.totalArousalMean += wordToAdd.getArousalMean();
		this.totalArousalSD += wordToAdd.getArousalSD();
		this.totalDominanceMean += wordToAdd.getDominanceMean();
		this.totalDominanceSD += wordToAdd.getDominanceSD();
		wordCount++;
	}


	public String toString() {
		return "average ValenceMean : " + (this.totalValenceMean) / wordCount + "\n" +
				"average ValenceSD : " + (this.totalValenceSD) / wordCount + "\n" + 
				"average totalArousalMean : " + (this.totalArousalMean) / wordCount + "\n" + 
				"average totalArousalSD : " + (this.totalArousalSD) / wordCount + "\n" + 
				"average totalDominanceMean : " + (this.totalDominanceMean) / wordCount + "\n" + 
				"average totalDominanceSD : " + (this.totalDominanceSD) / wordCount + "\n" + 
				"Total Words found: " + wordCount + "\n";
	}
	
	

}
