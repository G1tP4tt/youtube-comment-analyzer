package youtube_project;

import java.util.ArrayList;
import java.util.List;

public class Comment {

	Result commentResult;
	List<String> sentence = new ArrayList<>();

	public void addWordToComment(String word) {
		// TODO Auto-generated method stub
		sentence.add(word);
	}

	public boolean getNextWord() {

		return false;
	}

	public List<String> getWordsInComment() {
		// TODO Auto-generated method stub
		return this.sentence;
	}

	public void setTheResultForComment(Result theResult) {
		this.commentResult = theResult;
	}
	
	@Override
	public String toString() {
		String wordsInComment = printComment();
		return wordsInComment + "\n" + commentResult.toString() + "Total words in Comment: " + sentence.size();
	}

	private String printComment() {
		
		String theComment = new String();
		for(String s: sentence){
			theComment += s + " ";
		}
		return theComment;
	}
	
	

}
