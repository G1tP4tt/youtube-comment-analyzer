package youtube_project;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class WordFile {
	public ArrayList<Words> theFile;
	private Result theResult = new Result();
	Map<String, Words> wordMap = new HashMap<>();
	// we need to track the word cus we need the Variable in more than one
	// function
	String theWord;
	String theComment;
	List<Comment> commentsList = new ArrayList<>();

	public WordFile() {
		theFile = new ArrayList<Words>();
	}

	public void add(String theWord, int wordNo, int valenceMean, int valenceSD, int arousalMean, int arousalSD,
			int dominanceMean, int dominanceSD, String wordFrequency) {
		theFile.add(new Words(theWord, wordNo, valenceMean, valenceSD, arousalMean, arousalSD, dominanceMean,
				dominanceSD, wordFrequency));
	}

	public void addAll(ArrayList<Words> list) {
		theFile.addAll(list);
	}

	public void addAll(String fileName) {
		ArrayList<Words> toBeAdded = new ArrayList<Words>();

		try {
			Scanner fileIn = new Scanner(new File(fileName));
			int lineNum = 1;

			while (fileIn.hasNextLine()) {
				String line = fileIn.nextLine();

				Scanner lineIn = new Scanner(line);
				lineIn.useDelimiter(",");

				if (!lineIn.hasNext())
					throw new ParseException("Word", lineNum);
				String theWord = lineIn.next();

				if (!lineIn.hasNext())
					throw new ParseException("Word No.", lineNum);
				int wordNo = lineIn.nextInt();

				if (!lineIn.hasNext())
					throw new ParseException("Valence Mean", lineNum);
				double valenceMean = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Valence SD: ", lineNum);
				double valenceSD = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Arousal Mean:", lineNum);
				double arousalMean = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Arousal SD: ", lineNum);
				double arousalSD = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Dominance Mean: ", lineNum);
				double dominanceMean = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Dominance SD: .", lineNum);
				double dominanceSD = Double.parseDouble(lineIn.next());

				if (!lineIn.hasNext())
					throw new ParseException("Word Frequency: ", lineNum);
				String wordFrequency = lineIn.next();

				toBeAdded.add(new Words(theWord, wordNo, valenceMean, valenceSD, arousalMean, arousalSD, dominanceMean,
						dominanceSD, wordFrequency));

				lineNum++;
			}
		} catch (FileNotFoundException e) {
			System.err.println(e.getMessage() + " Nothing added.");
			return;
		} catch (ParseException e) {
			System.err.println(e.getLocalizedMessage() + " formatted incorrectly at line " + e.getErrorOffset()
					+ ". Nothing added.");
			return;
		}

		theFile.addAll(toBeAdded);
	}

	private static <ListType> void sort(ArrayList<ListType> list, Comparator<ListType> c) {
		for (int i = 0; i < list.size() - 1; i++) {
			int j, minIndex;
			for (j = i + 1, minIndex = i; j < list.size(); j++)
				if (c.compare(list.get(j), list.get(minIndex)) < 0)
					minIndex = j;
			ListType temp = list.get(i);
			list.set(i, list.get(minIndex));
			list.set(minIndex, temp);
		}
	}

	public ArrayList<Words> getOrderedByWords() {
		// public String getOrderedByWords() {
		// ArrayList<Words> theOne = new ArrayList<Words>();
		// theOne.addAll(theFile);
		OrderByWord theComparator = new OrderByWord();
		sort(theFile, theComparator);
		return theFile;
		/*
		 * String theWord; int wordNo; double valenceMean; double valenceSD;
		 * double arousalMean; double arousalSD; double dominanceMean; double
		 * dominanceSD; String wordFrequency; String result = ""; for(int i = 0;
		 * i < theOne.size(); i++) { theWord = theOne.get(i).getTheWord();
		 * wordNo = theOne.get(i).getWordNo(); valenceMean =
		 * theOne.get(i).getValenceMean(); valenceSD =
		 * theOne.get(i).getValenceSD(); arousalMean =
		 * theOne.get(i).getArousalMean(); arousalSD =
		 * theOne.get(i).getArousalSD(); dominanceMean =
		 * theOne.get(i).getDominanceMean(); dominanceSD =
		 * theOne.get(i).getDominanceSD(); wordFrequency =
		 * theOne.get(i).getWordFrequency(); result += theWord + " | " + wordNo
		 * + " | " + valenceMean + " | " + arousalMean + " | " + arousalSD +
		 * " | " + dominanceMean + " | " + dominanceSD + " | " + wordFrequency +
		 * "\n"; } return result;
		 */
	}

	protected class OrderByWord implements Comparator<Words> {
		public int compare(Words word1, Words word2) {
			String the1 = word1.getTheWord().toLowerCase();
			String the2 = word2.getTheWord().toLowerCase();
			int theResult = the1.compareTo(the2);
			return theResult;
		}
	}

	public String toString() {

		String theWord;
		int wordNo;
		double valenceMean;
		double valenceSD;
		double arousalMean;
		double arousalSD;
		double dominanceMean;
		double dominanceSD;
		String wordFrequency;

		String result = "";
		for (int i = 0; i < theFile.size(); i++) {
			theWord = theFile.get(i).getTheWord();
			wordNo = theFile.get(i).getWordNo();
			valenceMean = theFile.get(i).getValenceMean();
			valenceSD = theFile.get(i).getValenceSD();
			arousalMean = theFile.get(i).getArousalMean();
			arousalSD = theFile.get(i).getArousalSD();
			dominanceMean = theFile.get(i).getDominanceMean();
			dominanceSD = theFile.get(i).getDominanceSD();
			wordFrequency = theFile.get(i).getWordFrequency();
			result += theWord + " | " + wordNo + " | " + valenceMean + " | " + valenceSD + " | " + arousalMean + " | "
					+ arousalSD + " | " + dominanceMean + " | " + dominanceSD + " | " + wordFrequency + "\n";
		}
		return result;
	}

	/**
	 * This method creates a scanner and reads the input file.
	 * Calls other methods to analyze and print the results of each comment from the input file.
	 * 
	 * @param words
	 *            --> the HashMap where all words (former myFile.txt) are stored
	 *            in.
	 */
	public void doPatriceStuff(Map<String, Words> words) {
		// sets up member hashMap cus we need it in several functions.
		wordMap = words;

		URL url = WordFile.class.getResource("testIn.txt");
		File file = new File(url.getPath());
		Scanner in = null;

		try {
			in = new Scanner(file);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		splitIntoSeperateComments(in);

		analyzeAllComments();

		printAllCommentResults();
		in.close();

	}

	/**
	 * For testing purposes. Prints the Comment at commentsList[index] to stdIN.
	 * 
	 * @param index
	 *            the index of the comment which should be printed.
	 */
	private void printSpecificComment(int index) {

		System.out.println("Comment Number: " + index + "\n" + commentsList.get(index));

	}

	/**
	 * Prints all Comments to stdIN. In reverse order. So comment 180 is printed
	 * first --> its at index[0]
	 * 
	 */
	private void printAllCommentResults() {

		int i = 1;
		for (Comment theComment : commentsList) {
			System.out.println("Comment Number " + i + ": \n" + theComment);
			i++;
		}
	}

	/**
	 * Method appends each word to a <-- Comment --> until a Number followed by
	 * a dot is found ([\d]+[.]). (Nooooot the best way though --> too many
	 * Comments are found in list)
	 * 
	 * @param in
	 *            A Scanner object which needs to be separated.
	 */
	private void splitIntoSeperateComments(Scanner in) {

		int i = 0;

		Comment theComment = new Comment();

		while (in.hasNext()) {
			String word = new String();

			word = in.next();
			
			if (!word.matches("[\\d]+[.]")) {
				theComment.addWordToComment(word);

			} else {
				commentsList.add(theComment);
				theComment = new Comment();
				i++;

			}

		}

		System.out.println(i + " Comments found in file.");
	}

	/**
	 * Anaylzes each comment and sets it's Result. Iterates through commentsList
	 * and checks every word, for each comment.
	 * 
	 * 
	 */
	private void analyzeAllComments() {
		for (Comment commentToAnalyze : commentsList) {

			theResult = new Result();

			List<String> sentence = commentToAnalyze.getWordsInComment();

			for (String word : sentence) {
				checkWord(word);
			}

			commentToAnalyze.setTheResultForComment(theResult);
			// Whole Comment is scanned.
		}
	}

	/**
	 * Checks if theWord is in the WordMap. I didnt check for the ending in
	 * particular. I just erase the last char. And check again (max. 3 times).
	 * 
	 * EXAMPLE: Even when i cut the 'ing' from 'believing', it would come out as
	 * 'believ' --> Stanford nlp should transform the whole text before
	 * everything and then we would check the words. --> So we would only have
	 * to search for the word once, because we know that the word we are looking
	 * for is the base form of the word.
	 * 
	 * for each word we need max O(3) accesses. (Which is still so much quicker
	 * than binary searching the file) Hope you like it. If you don't, we can
	 * easily implement a method which checks the specific endings.
	 * 
	 * @param wordToCheck
	 */
	private void checkWord(String wordToCheck) {
		theWord = wordToCheck.replaceAll("[,.!?()/\']", "");

		int i = 0;

		// i --> is used for cutting the end of the word. (3 times max. in this
		// case)
		while (!theWord.equals("NaN") && i != 4) {

			if (wordMap.containsKey(theWord)) {

				collectTextValues(wordMap.get(theWord));

			} else {
				// System.out.println("The word: '" + theWord + "' was not found
				// in WordMap, trying to transform.");
				theWord = transformWord(theWord);
				i++;
			}

		}

	}

	/**
	 * @param theWord
	 * @return the word without the last char of @param theWord
	 */
	private String transformWord(String theWord) {

		// Check for irregular verbs here...
		// if we would do everything with if/else statements like in your
		// picture, card would transform to car... which is wrong.
		// i think we need the standford NLP lib. Otherwhise the result will
		// never be exact..

		if (theWord.length() < 3) {
			return "NaN";
		} else {
			return theWord.substring(0, theWord.length() - 1);
		}
	}

	/**
	 * Calls @method addUp(wordToAdd). Sets theWord to "NaN" (so we know that we
	 * don't have to continue searching for the Word)
	 * 
	 * @param wordToAdd
	 */
	private void collectTextValues(Words wordToAdd) {
		// System.out.println("Adding " + wordToAdd.getTheWord() + " to
		// Result.");
		theResult.addUp(wordToAdd);
		theWord = "NaN";

	}

	public ArrayList<Words> getFile() {
		// TODO Auto-generated method stub
		return theFile;
	}
}
